This little project is meant to teach me the basics of Flask
------------------------------------------------------------

Trying to work along the [Official Flask tutorial](http://flask.pocoo.org/docs/1.0/tutorial/) but I'm getting an error when trying to execute "flask init\_db". The error says: `Error: No such command "init-db".`

My editor shows some error in \_\_init\_\_.py on line 27 (`from python_flask import db`)
```python
    # register the database commands
    from python_flask import db
    db.init_app(app)
```

I'll be updating this README file along with the content from now on, detailing where I got stuck and how it got solved.

A full traceback is needed for further debugging.