
import myapp.factory

app = myapp.factory.create_app()


if __name__ == '__main__':
    app.run(debug=True)


